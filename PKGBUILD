# Maintainer: Your Name <youremail@domain.com>
pkgname=oddjob
pkgver=0.34.7
pkgrel=1
pkgdesc="A D-Bus service which runs odd jobs on behalf of client applications"
arch=('x86_64')
url="https://releases.pagure.org/oddjob/"
license=('BSD')
depends=(
    'bash'
    'dbus'
    'glibc'
    'libselinux'
    'libxml2'
    'pam'
    'systemd'
)
makedepends=(
    'cyrus-sasl'
    'krb5'
    'libldap'
)
backup=()
options=()
install="$pkgname.install"
source=(
    "https://releases.pagure.org/$pkgname/$pkgname-$pkgver.tar.gz"{,.asc}
)
sha256sums=(
    'e16ce096161265fb6838a64e325015b0f79ffa9b920e79287d8cae488f37dab0'
    '453d934443ad053cff4e0a3545ce84a4736c012ee2a6e750a9c06b0ea438873b'
)
validpgpkeys=(
    '0E63D716D76AC080A4A33513F40800B6298EB963'
)

prepare() {
    cd "$pkgname-$pkgver"

    autoreconf -vfi
}

build() {
    local configure_options=(
        --prefix=/usr
        --sysconfdir=/etc
        --sbindir=/usr/bin
        --libexecdir="/usr/lib/${pkgname}"
        --disable-static
        --enable-pie
        --enable-now
        --without-python
        --enable-systemd
        --disable-sysvinit
    )

    cd "$pkgname-$pkgver"

    ./configure "${configure_options[@]}"

    make
}

package() {
    cd "$pkgname-$pkgver"

    make DESTDIR="$pkgdir/" install

    install -Dpm644 'COPYING' -t "${pkgdir}/usr/share/licenses/${pkgname}/"
}
